from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        'todo_list_detail': detail
    }
    return render(request, 'todos/detail.html', context)

def create_list(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', list.id)
    else:
        form = TodoListForm()
        context = {
            'form': form,
        }
    return render(request, 'todos/create.html', context)

def edit_list(request, id):
    detail = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=detail)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = TodoListForm(instance=detail)
    context = {
        'form': form,
        'todo_list_detail': detail,
    }
    return render(request, 'todos/edit.html', context)

def delete_list(request, id):
  delete = TodoList.objects.get(id=id)
  if request.method == "POST":
    delete.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', item.list.id)
    else:
        form = TodoItemForm()
        context = {
            'form': form,
        }
    return render(request, 'todos/create.html', context)

def edit_todo_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = TodoItemForm(instance=item)
    context = {
        'form': form,
    }
    return render(request, 'todos/update.html', context)
